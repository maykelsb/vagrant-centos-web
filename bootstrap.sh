#!/usr/bin/env bash

echo ">>> Adicionando repositorio webtatic"
sudo rpm -Uvh https://mirror.webtatic.com/yum/el6/latest.rpm
echo ">>> Adicionando repositorio do postgresql"
sudo rpm -Uvh http://yum.postgresql.org/9.3/redhat/rhel-6-x86_64/pgdg-centos93-9.3-1.noarch.rpm

echo ">>> Atualizando os pacotes da maquina"
sudo yum update -y

#echo ">>> Instalando telnet"
#sudo yum -y install telnet

echo ">>> Instalando postgresql-server"
sudo yum -y install postgresql93-server postgresql93-contrib
echo ">>> Iniciando o banco de dados postgresql93"
sudo service postgresql-9.3 initdb
echo ">>> Ativando inicializaco automatica"
sudo chkconfig postgresql-9.3 on

echo ">>> Iniciando o servico postgresql93"
sudo service postgresql-9.3 start
echo ">>> Definindo nova senha para o usuario postgres no banco"
sudo su postgres << EOF
psql -c "ALTER USER postgres WITH ENCRYPTED PASSWORD 'postgres';"
EOF

echo ">>> Ativando conexoes remotas"
sudo printf "listen_addresses = '*'\nport = 5432\n" >> /var/lib/pgsql/9.3/data/postgresql.conf
echo ">>> Mudando autenticacao para md5"
sudo sed -i 's/peer$/md5/g' /var/lib/pgsql/9.3/data/pg_hba.conf
sudo sed -i 's/ident$/md5/g' /var/lib/pgsql/9.3/data/pg_hba.conf
# Acesso negado
#echo ">>> Adicionando nova regra de conexao no pg_hba.conf"
#sudo printf "\nhost all all 10.0.2.2/32 md5\n" >> /var/lib/pgsql/9.3/data/pg_hba.conf

echo ">>> Instalando apache"
sudo yum -y install httpd httpd-devel
echo ">>> Ativando inicializacao automatica"
sudo chkconfig httpd on

echo ">>> Criando links de diretorios de projetos"
echo "Dialogos"
sudo ln -fs /vagrant/dialogos /var/www/dialogos

echo ">>> Ajustando permissoes do diretório /var/www"
sudo chmod 755 /var/www

echo ">>> Copiando configuracoes de vhosts para o servidor"
sudo cp /vagrant/vhosts.conf /etc/httpd/conf.d/vhosts.conf

echo ">>> Criando diretorios de log"
sudo mkdir -p /vagrant/httpd/errors

echo ">>> Instalando PHP5.5"
sudo yum -y install php55w php55w-common php55w-pdo php55w-pecl-xdebug php55w-pgsql php55w-xml

#echo ">>> Instalando phpPgAdmin"
#sudo yum -y install phpPgAdmin
#echo ">>> Liberando acesso externo ao phpPgAdmin"
#sudo sed -i 's/Deny from all/Allow from all/g' /etc/httpd/conf.d/phpPgAdmin.conf
#echo ">>> Desligando autenticacao segura no phpPgAdmin"
#sudo sed -i "s/\$conf\['extra_login_security'\] = true;/\$conf\['extra_login_security'\] = false;/g" /etc/phpPgAdmin/config.inc.php


echo ">>> Iniciando o servidor web"
sudo service httpd start

